var wordbase = ["able","across","against","agree","all","already","although","an","and","anticipates","any","approach","approach","as","asking","assessed","assessment","at","august","authentication","banking","based","basis","be","been","being","best","by","catered","choice","clarification","clarifying","come","concludes","confirming","consistent","consultation","consulted","continually","continued","conversion","date","delay","delaying","deleting","desirable","developed","did","directive","draft","due","either","erroneous","european","examples","existing","expressed","few","final","finally","first","for","forthcoming","forum","fraud","from","front","furthermore","future","given","group","guide","guidelines","guidelines","had","has","high","higher","how","implementation","implemented","in","including","input","instead","intended","internet","into","is","issue","issuing","its","january","large","later","legal","levels","loads","made","majority","meaning","member","minor","minority","modifications","more","necessary","negotiations","not","number","observed","october","of","on","one-step","ongoing","only","onwards","option","option","or","original","paper","particular","pass","pay","payment","payments","payments","payments","plausible","potentially","practice","preference","preferences","proposed","provide","published","questions","received","recommendations","recommendations","references","regard","relevance","remaining","re-numbering","required","requirements","respondents","response","responses","retail","revision","schemes","second-best","secure","security","see","services","set","should","significant","so","solely","solid","solution","some","sought","stage","stakeholder","stakeholders","standards","stated","states","stringent","strong","substance","surrounding","text","that","the","their","therefore","these","they","through","to","transposition","two","two-step","under","until","wait","were","which","will","with","would"];

function parseFeed(feedURL) {
  rfeed = jQuery.parseJSON(get_feed_json("http://www.postimees.ee/rss/"));
  items = rfeed.feed.entry; // rss.channel.item; 
  console.log(items); 
}

$(document).ready(function() {
  var sectionClass;
  var $container = $('.firstSection');
  
  for (var i = 0; i < 20; i++) {
    sectionClass = "section";
    var addedArticle = genArticle();      
    $(sectionClass).html($(sectionClass).html() + addedArticle);                                         
  }

  $container.masonry({ 
    itemSelector: 'article', 
    "isFitWidth": true, 
    columnWidth: 320,
    "percentPosition": true,
    "gutter": 10
  }).on();
    
  $container.imagesLoaded(function() {
    $container.masonry().layout();
  });
});

$(window).resize(function() {
  // uncomment for working copy of masonry
  $('section').masonry().layout();
});

$(document).on("click", "#generate", function() {
  // parseFeed($("#feedURL").val());
  alert($(".firstSection").html());
});

$(document).on("click", ".item", function(event) {
  var articlePopup = $('.articlePopover');
  articlePopup.html(genCustomArticle(10, 15, 200, "popover"));
  
  if (articlePopup.css("display") == "none") {
    /* articlePopup.css('left', 120); // event.pageX); // <<< use pageX and pageY
    /* articlePopup.css('top', 50); // event.pageY);*/
    articlePopup.css('height', '100%');
    articlePopup.css('max-h', '100%');    
    articlePopup.css('position', 'fixed'); // <<< also make it absolute!
    articlePopup.css('zIndex', '1');    
    // articlePopup.text("article ID: " + this.id)
    $(".articlePopover").animate(
        { 
          width: ["toggle", "swing"],
          height: ["toggle", "swing"],
          opacity: "toggle"
        },
        10
    );
  } else {
    $(".articlePopover").hide();
  }
});

$(document).on("click", ".articlePopover", function() {
  // alert(this.id);
  $(".articlePopover").hide();
});

function randBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

function genSentence(maxWords) {
  var sentence = "",  sentenceLength = randBetween(4, maxWords); // minimum length 4
  for (var i = 0; i < sentenceLength; i++) {
    sentence = sentence + wordbase[randBetween(0, wordbase.length - 1)]; 
    if (i < sentenceLength - 1) sentence += " ";
    else sentence += ". ";
  } 
  return sentence.capitalizeFirstLetter(); 
}

function genArticle() {
  return genCustomArticle(7, 3, 7, "item");
}

function genCustomArticle(titleLength, articleLengthMin, articleLengthMax, articleClass) {
  if (articleClass == "undefined") articleClass = "item";
  var articleTitle = genSentence(titleLength);
  var articleId = randBetween(1, 9999);
  var articleContent = "";
  var imageClass = "";

  switch (randBetween(1, 4)) {
    case 1:
      imageClass = "nature";
      break;
    case 2:
      imageClass = "sports";
      break;
    case 3:
      imageClass = "business";
      break;
    case 4:
      imageClass = "technics";
      break;
  }

  var articleImage = "<img class='" + articleImage 
                   + "Image' src=\"http://lorempixel.com/500/300/"
                   + imageClass + "/?" + Math.floor(Math.random() * 13000) 
                   + "\"/>";

  // minimum length - 3 senteces
  var articleLength = randBetween(articleLengthMin, articleLengthMax);

  for (var i = 0; i <= articleLength; i++) {                    
    articleContent += genSentence(7);                                  
  };  

  return "<article class='" + articleClass + "' id=\"" + articleId
       + "\">" + articleImage + "<h1>" + articleTitle + "</h1><p>"
       + articleContent + "</p></article>";
}